<?php 
	/* Template Name: Home */
	get_header();

?>
<div>
<?php	 
	if(have_posts()):
		while(have_posts()):
		the_post();
			echo the_content();
		endwhile;
	endif; 
?>
	
</div>


<?php get_footer(); ?>
