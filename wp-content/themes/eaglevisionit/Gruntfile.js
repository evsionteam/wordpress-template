module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compass: {
            main: {
                options: {
                    sassDir: './assets/src/scss/',
                    cssDir: './assets/build/css',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            }
        },

        concat: {
           vendor: {
                src: [
                    './node_modules/bootstrap/dist/css/bootstrap.css',
                ],
                dest: './assets/build/css/vendor.css',
            }
        },
       
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourcemap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './assets/build/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: './assets/build/css/',
                    ext: '.min.css'
                }]
            }
        },

      
        watch: {
            sass: {
                files: [
                    './assets/src/scss/*.scss',
                    './assets/src/scss/**/*.scss',
                    './assets/src/scss/**/**/.scss',

                ],
                task: ['css']
            },
            img: {
                files: [
                    './assets/src/img/*',
                ],
                tasks: ['img'],
            },
            js: {
                files: [
                    './assets/src/js/*.js',
                ],
                task: ['js'],
            },
        }



    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks("grunt-wp-i18n");
   
    grunt.registerTask('css', ['compass','cssmin']);
    grunt.registerTask('default', ['compass','concat','cssmin']); 
    grunt.registerTask('deploy', ['compass', 'concat','cssmin']); 
    // grunt.registerTask('css', ['compass', 'cssmin']);
    // grunt.registerTask('js', ['concat:vendorsJS', 'copy','uglify']);
    // grunt.registerTask('img', ['copy']);
    // grunt.registerTask('default', ['compass', 'concat', 'copy']);
    // grunt.registerTask('deploy', ['compass', 'concat', 'copy', 'uglify', 'cssmin' ]); 
}