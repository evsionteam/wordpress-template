<?php 

get_header();

while(have_posts()){
	the_post();
	?>
	<h1><a href="<?php echo get_page_link(get_the_ID()); ?>" ><?php echo get_the_title(); ?></a></h1>
	<h3><?php echo get_the_content(); ?></h3>
	
	<?php
}

get_footer();