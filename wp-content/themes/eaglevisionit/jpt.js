module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),




        cssmin: {
            minify: {
                expand: true,
                cwd: 'release/css/',
                src: ['.css', '!.min.css'],
                dest: 'release/css/',
                ext: '.min.css'
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './assets/build/css/',
                    src: ['*.css', '!*.min.css'],
                    dest: './assets/build/css/',
                    ext: '.min.css'
                }]
            }
        },

        uglify: {
            options: {
                report: 'gzip'
            },
            whole: {
                files: [{
                    expand: true,
                    cwd: './assets/src/js/',
                    src: '*.js',
                    dest: './assets/build/js/',
                    rename: function (dst, src) {
                        return dst + '/' + src.replace('.js', '.min.js')
                    }
                }, {
                    expand: true,
                    cwd: './assets/build/js/modules/',
                    src: ['*.js', '!*.min.js'],
                    dest: './assets/build/js/modules/',
                    rename: function (dst, src) {
                        return dst + '/' + src.replace('.js', '.min.js')
                    }
                }]
            },
            poperjs: {
                src: ["./assets/build/js/modules/popper.js"],
                dest: "./assets/build/js/modules/popper.min.js"
            },
            adminjs: {
                src: ["./assets/build/js/admin/*.js"],
                dest: "./assets/build/js/admin/admin-script.min.js"
            }
        },

        copy: {
            img: {
                files: [{
                    expand: true,
                    cwd: './assets/src/img',
                    src: '**',
                    dest: './assets/build/img/',
                    filter: 'isFile'
                }]
            },

            fonts: {
                files: [{
                    expand: true,
                    cwd: './assets/src/fonts',
                    src: '**',
                    dest: './assets/build/fonts/',
                    filter: 'isFile'
                }]
            },

        },

        watch: {
            sass: {
                files: [
                    './assets/src/scss/*.scss',
                    './assets/src/scss/**/*.scss',
                    '../assets/src/scss/**/**/*.scss'
                ],
                tasks: ['css']
            },

            img: {
                files: [
                    './assets/src/img/*'
                ],
                tasks: ['img']
            },

            js: {
                files: [
                    './assets/src/js/*.js'
                ],
                tasks: ['js']
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks("grunt-wp-i18n");

    grunt.registerTask('css', ['compass', 'cssmin']);
    grunt.registerTask('js', ['concat:vendorsJS', 'copy', 'uglify']);
    grunt.registerTask('img', ['copy']);
    grunt.registerTask('default', ['compass', 'concat', 'copy']);
    grunt.registerTask('deploy', ['compass', 'concat', 'copy', 'uglify', 'cssmin']);
}