<?php

add_action( 'wp_enqueue_scripts', 'wp_template_load_scripts' );
function wp_template_load_scripts() {
	$suffix = '.min';
	if(defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){ 
		$suffix = '';
	}

	$assests_base = get_template_directory_uri().'/assets/build';

	wp_enqueue_style( 'wp-template-vendor', $assests_base . '/css/vendor'.$suffix.'.css', array(), null );
	wp_enqueue_style( 'wp-template-main', $assests_base . '/css/main'.$suffix.'.css', array(), null );


	// wp_register_script( 'require', $assests_base . '/js/require'.$suffix.'.js', array(), null, true );
	// wp_enqueue_script( 'require-config', $assests_base . '/js/config'.$suffix.'.js', array('require'), null, true );
	// $localized_var = lustflyllt_get_localized_variables();
	// wp_localize_script( 'require-config', 'LUSTFLYLLT', $localized_var );

	// lustflyllt_load_style(array('home', 'kontakt', 'bokaboard', 'lunch', 'gallery','brolopp','events','wedding','hochzeit','reserve-table','einen-tisch-reservieren','contact'));

}	
