<?php
// Register Custom Post Type
add_action( 'init', 'eaglevisionit_event_post_type', 0 ); 
function eaglevisionit_event_post_type() {

    $labels = array(
        'name'                  => _x( 'Events', 'Post Type General Name', 'eaglevisionit' ),
        'singular_name'         => _x( 'Lunch', 'Post Type Singular Name', 'eaglevisionit' ),
        'menu_name'             => __( 'Events', 'eaglevisionit' ),
        'name_admin_bar'        => __( 'Events', 'eaglevisionit' ),
        'archives'              => __( 'Item Archives', 'eaglevisionit' ),
        'parent_item_colon'     => __( 'Parent Item:', 'eaglevisionit' ),
        'all_items'             => __( 'All Items', 'eaglevisionit' ),
        'add_new_item'          => __( 'Add New Item', 'eaglevisionit' ),
        'add_new'               => __( 'Add New', 'eaglevisionit' ),
        'new_item'              => __( 'New Item', 'eaglevisionit' ),
        'edit_item'             => __( 'Edit Item', 'eaglevisionit' ),
        'update_item'           => __( 'Update Item', 'eaglevisionit' ),
        'view_item'             => __( 'View Item', 'eaglevisionit' ),
        'search_items'          => __( 'Search Item', 'eaglevisionit' ),
        'not_found'             => __( 'Not found', 'eaglevisionit' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'eaglevisionit' ),
        'featured_image'        => __( 'Featured Image', 'eaglevisionit' ),
        'set_featured_image'    => __( 'Set featured image', 'eaglevisionit' ),
        'remove_featured_image' => __( 'Remove featured image', 'eaglevisionit' ),
        'use_featured_image'    => __( 'Use as featured image', 'eaglevisionit' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'eaglevisionit' ),
        'items_list'            => __( 'Items list', 'eaglevisionit' ),
        'items_list_navigation' => __( 'Items list navigation', 'eaglevisionit' ),
        'filter_items_list'     => __( 'Filter items list', 'eaglevisionit' ),
    );
    $args = array(
        'label'                 => __( 'Events', 'eaglevisionit' ),
        'labels'                => $labels,
        'supports'              => array( 'title','thumbnail','editor' ),
        'hierarchical'          => false,
        'public'                => true,
        'menu_icon'             => 'dashicons-calendar-alt',
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'rewrite'            => array( 'slug' => 'events' ),
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'events', $args );

}
